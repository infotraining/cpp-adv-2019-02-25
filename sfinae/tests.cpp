#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <optional>

using namespace std;
using namespace Catch::Matchers;


namespace Explain
{
	template <bool Condition, typename T = void>
	struct EnableIf
	{
		using type = T;
	};

	template <typename T>
	struct EnableIf<false, T>
	{};
}


template <typename T>
typename Explain::EnableIf<is_floating_point_v<T>, optional<T>>::type calculate(const T& data)
{
	cout << "Calculate for floating points..." << endl;
	return T{ 3.14 };
}

template <typename T>
enable_if_t<is_integral_v<T>, long> calculate(const T& data)
{
	cout << "Calculate for integers..." << endl;

	return 42L;
}

template <typename T, typename TResult = enable_if_t<is_array_v<T>, long>>
TResult calculate(const T& data)
{
	cout << "Calculate for arrays..." << endl;

	return 42L;
}

TEST_CASE("sfinae")
{   
	static_assert(is_void_v<Explain::EnableIf<(sizeof(int) == 4)>::type>);
	//static_assert(is_void_v<Explain::EnableIf<(sizeof(int) == 8)>::type>); // ERROR

	calculate(3.14);
	calculate(3.14f);

	calculate(10);
	short x = 10;
	calculate(x);

	int tab[] = { 1, 2, 3 };
	calculate(tab);
}

template <typename T>
class UniquePtr
{
	T* ptr_;
public:
	explicit UniquePtr(T* ptr) : ptr_{ ptr }
	{}

	~UniquePtr() { delete ptr_; }

	UniquePtr(const UniquePtr&) = delete;
	UniquePtr& operator=(const UniquePtr&) = delete;

	T& operator*() const
	{
		return *ptr;
	}

	enable_if_t<is_class_v<T>, T*> operator->() const
	{
		return ptr_;
	}
};