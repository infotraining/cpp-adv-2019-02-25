#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <iterator>

using namespace std;
using namespace Catch::Matchers;

template <typename Iter>
void advance_iter_impl(Iter& it, int steps, input_iterator_tag)
{
	cout << "Generic advance_iter" << endl;

	for (int i = 0; i < steps; ++i)
		++it;
}

template <typename Iter>
void advance_iter_impl(Iter& it, int steps, random_access_iterator_tag)
{
	cout << "Random Access Iterator advance_iter" << endl;

	it += steps;
}

template <typename Iter>
void advance_iter(Iter& it, int steps)
{
	advance_iter_impl(it, steps, typename iterator_traits<Iter>::iterator_category{});
}

namespace Cpp17
{
	template <typename Iter>
	void advance_iter(Iter& it, int steps)
	{
		if constexpr (is_same_v<typename iterator_traits<Iter>::iterator_category, random_access_iterator_tag>)
		{
			cout << "Random Access Iterator advance_iter" << endl;
			it += steps;
		}
		else // if constexpr(is_base_of_v<input_iterator_tag, typename iterator_traits<Iter>::iterator_category>)
		{
			cout << "Generic advance_iter" << endl;
			for (int i = 0; i < steps; ++i)
				++it;
		}
	}
}

TEST_CASE("test")
{
	SECTION("list")
	{
		list lst = { 1, 2, 3, 4, 5 };

		auto it = begin(lst);

		Cpp17::advance_iter(it, 3);

		REQUIRE(*it == 4);
	}

	SECTION("vector")
	{
		vector vec = { 1, 2, 3, 4, 5 };

		auto it = begin(vec);

		Cpp17::
			advance_iter(it, 3);

		REQUIRE(*it == 4);
	}
}