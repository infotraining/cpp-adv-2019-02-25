#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <functional>

using namespace std;
using namespace Catch::Matchers;

namespace Step1
{
	template <typename T, typename TResult>
	TResult my_accumulate(const T* first, const T* last, TResult init)
	{
		for (auto it = first; it != last; ++it)
			init += *it;

		return init;
	}
}


namespace Step2
{
	template <typename T>
	struct AccumulationTraits
	{
		using AccumulatorType = T;
		static constexpr AccumulatorType zero{};
	};

	template <>
	struct AccumulationTraits<uint8_t>
	{
		using AccumulatorType = unsigned int;
		static constexpr AccumulatorType zero{};
	};

	template <>
	struct AccumulationTraits<double>
	{
		using AccumulatorType = double;
		static constexpr AccumulatorType zero{};
	};

	template <>
	struct AccumulationTraits<float>
	{
		using AccumulatorType = double;
		static constexpr AccumulatorType zero{};
	};

	template <>
	struct AccumulationTraits<string>
	{
		using AccumulatorType = string;
		static constexpr const char* zero = "string: ";
	};

	struct Sum
	{
		template <typename T1, typename T2>
		static void accumulate(T1& left, const T2& right)
		{
			left += right;
		}
	};

	struct Multiply
	{
		template <typename T1, typename T2>
		static void accumulate(T1& left, const T2& right)
		{
			left *= right;
		}
	};

	struct ScalarTraits
	{
		using AccumulatorType = int;
		static constexpr int zero = 1;
	};


	template <
		typename InpIter, 
		typename AccTraits = AccumulationTraits<typename iterator_traits<InpIter>::value_type>,
		typename AccPolicy = Sum>
	typename AccTraits::AccumulatorType my_accumulate(InpIter first, InpIter last)
	{
		using TResult = typename AccTraits::AccumulatorType;

		TResult total = AccTraits::zero;

		for (auto it = first; it != last; ++it)
			AccPolicy::accumulate(total, *it);

		return total;
	}
}

TEST_CASE("my accumulate tests")
{
	SECTION("uint8")
	{
		vector tab = { 1, 2, 3, 255 };

		auto result = Step2::my_accumulate(begin(tab), end(tab));

		REQUIRE(result == 261);
	}

	SECTION("double")
	{
		float tab[] = { 1, 3.14, 2, 3.0 };

		sort(begin(tab), end(tab), std::greater{});

		auto result = Step2::my_accumulate(begin(tab), end(tab));
		static_assert(is_same_v<decltype(result), double>);

		REQUIRE(result == Approx(9.14));
	}

	SECTION("string")
	{
		string words[] = { "one", "two", "three" };
		auto result = Step2::my_accumulate(begin(words), end(words));

		REQUIRE(result == "string: onetwothree"s);
	}
}

