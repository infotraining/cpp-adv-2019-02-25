#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <tuple>

using namespace std;
using namespace Catch::Matchers;


template <typename T, typename... Args>
unique_ptr<T> my_make_unique(Args&&... args)
{
	return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

void print()
{
	cout << "\n";
}

template <typename Head, typename... Tail>
void print(const Head& head, const Tail& ... tail)
{
	cout << head << " ";
	print(tail...); // recursive call for tail
}

namespace Cpp17
{
	template <typename Head, typename... Tail>
	void print(const Head& head, const Tail& ... tail)
	{
		cout << head << " ";

		if constexpr (sizeof...(tail) > 0)
			print(tail...);
		else
			cout << '\n';
	}
}

TEST_CASE("variadic templates")
{
	auto ptr1 = my_make_unique<tuple<int, string>>(1, "one"s);

	REQUIRE(get<0>(*ptr1) == 1);
	REQUIRE(get<1>(*ptr1) == "one"s);

	Cpp17::print(1, 3.14, "text", "hello"s);
}