#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace Catch::Matchers;

template <typename T>
struct TypeSize
{
	static constexpr size_t value = sizeof(T);
};



template <typename C>
struct ElementT
{
	using type = typename C::value_type;
};

template <typename T, size_t N>
struct ElementT<T[N]>
{
	using type = T;
};

template <typename C>
using ElementT_t = typename ElementT<C>::type;



template <typename Container>
void process(const Container& c)
{
	using T = ElementT_t<Container>;	

	using T = decay_t<decltype(*begin(c))>;

	//...
}

TEST_CASE("meta-function")
{
	static_assert(TypeSize<int>::value == 4);

	int tab[10] = { 1, 2, 3, 4 };

	process(tab);
}

namespace Explain
{
	template <typename T, T v>
	struct IntegralConstant
	{
		static const T value = v;
		using value_type = T;
	};

	template <bool v>
	using BoolConstant = IntegralConstant<bool, v>;

	using TrueType = BoolConstant<true>;
	using FalseType = BoolConstant<false>;

	// type trait - IsVoid
	template <typename T>
	struct IsVoid : FalseType
	{};

	template<>
	struct IsVoid<void> : TrueType
	{};


	// type trait - IsPointer
	template <typename T>
	struct IsPointer : FalseType
	{};

	template <typename T>
	struct IsPointer<T*> : TrueType
	{};
}

template <typename T>
void use(const T& item)
{
	//...

	if constexpr (Explain::IsPointer<T>::value)
	{
		cout << "Delete item... " << item << endl;
		delete item;
	}
}


TEST_CASE("explain type traits")
{
	using namespace Explain;

	static_assert(Explain::IntegralConstant<int, 4>::value == 4);
	static_assert(Explain::BoolConstant<true>::value);

	static_assert(!IsVoid<int>::value);
	static_assert(IsVoid<void>::value);

	auto ll = [](const auto & item)
	{
		using T = decay_t<decltype(item)>;
		T var{};
	};

	int tab[] = { 1, 2, 3 };
	use(tab);

	int* ptr = new int[10];
	use(ptr);
}
