#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace Catch::Matchers;

string full_name(const string& first, const string& last)
{
	return first + " " + last;
}

TEST_CASE("reference binding")
{
	string name = "Jan";

	SECTION("C++98")
	{
		SECTION("l-value ref can be bound to l-value")
		{
			string& ref_name = name;
		}

		SECTION("r-value can be bound to l-value ref with const")
		{
			const string& ref_full_name = full_name(name, "Kowalski");
		}
	}

	SECTION("C++11")
	{
		SECTION("r-value can be bound to r-value ref")
		{
			string&& ref_full_name = full_name(name, "Kowalski");
			ref_full_name[0] = 'P';

			std::cout << ref_full_name << '\n';
		}

		SECTION("l-value can not be bound to r-value ref")
		{
			//string&& ref_name = name; // ERROR - ill formed
		}
	}
}

class MyVector
{
	int* array_;
	size_t size_;

	void print_items() const
	{
		std::cout << "({ ";
		for (size_t i = 0; i < size_; ++i)
			std::cout << array_[i] << " ";
		std::cout << "})\n";
	}
public:
	using iterator = int*;
	using const_iterator = const int*;

	MyVector(std::initializer_list<int> lst)
		: array_{ new int[lst.size()] }, size_{ lst.size() }
	{
		std::copy(lst.begin(), lst.end(), array_);

		std::cout << "MyVector";
		print_items();

	}

	MyVector(const MyVector & source)
		: array_{ new int[source.size_] }, size_{ source.size_ }
	{
		std::copy(source.begin(), source.end(), array_);

		std::cout << "MyVector-cc ";
		print_items();
	}

	MyVector(MyVector && source) noexcept
		: array_{ source.array_ }, size_{ source.size_ }
	{
		source.array_ = nullptr;
		source.size_ = 0; // extra safety

		std::cout << "MyVector-mv ";
		print_items();
	}

	MyVector& operator=(const MyVector & source)
	{
		if (&source != this)
		{
			delete[] array_;
			array_ = new int[source.size_];
			size_ = source.size_;

			std::copy(source.begin(), source.end(), array_);
		}

		std::cout << "MyVector-cc= ";
		print_items();

		return *this;
	}

	MyVector& operator=(MyVector && source) noexcept
	{
		if (&source != this)
		{
			delete[] array_;
			array_ = source.array_;
			size_ = source.size_;

			source.array_ = nullptr;
			source.size_ = 0;
		}

		std::cout << "MyVector-mv= ";
		print_items();

		return *this;
	}

	~MyVector()
	{
		std::cout << "~Vector";
		print_items();

		delete[] array_;
	}

	iterator begin()
	{
		return array_;
	}

	iterator end()
	{
		return array_ + size_;
	}

	const_iterator begin() const
	{
		return array_;
	}

	const_iterator end() const
	{
		return array_ + size_;
	}

	size_t size()  const
	{
		return size_;
	}
};

TEST_CASE("move semantics for vector")
{
	int tab[] = { 1, 2, 3, 4 };
	MyVector vec = { 1, 2, 3, 4 };

	MyVector dest = move(vec);

	vec = MyVector{ 1, 2, 3 };

	REQUIRE(dest.size() == 4);
	REQUIRE(vec.size() == 3);

	auto expected = { 1, 2, 3, 4 };
	REQUIRE(std::equal(dest.begin(), dest.end(), expected.begin()));
}

MyVector load_data(const std::string & file_name)
{
	MyVector data = { 1, 2, 3, 4 };

	//...

	return data;
}

TEST_CASE("using MyVector")
{
	MyVector vec = load_data("data.txt");

	cout << "\n\n%%%%%%%%%%%%%%%%%%%%%%%\n";

	std::vector<MyVector> stl_vec;

	stl_vec.push_back(load_data("one.txt"));
	stl_vec.push_back(MyVector{ 1, 7, 7, 8 });
	stl_vec.push_back(MyVector{ 1, 7, 7, 9 });
	stl_vec.push_back(std::move(vec));
}

struct Data
{
	vector<int> row1;
	MyVector row2;
	string name;

	Data(const vector<int>& r1, const MyVector& r2, const string& name)
		: row1(r1), row2(r2), name(name)
	{}


	Data(const Data&) = default;
	Data& operator=(const Data&) = default;
	Data(Data&&) = default;
	Data& operator=(Data&&) = default;

	virtual void print() const 
	{
		//...
	}

	virtual ~Data() = default;
};

struct BigData : Data
{
	string text;

	void print() const override
	{
		//...
	}
};


TEST_CASE("Data with move semantics")
{
	cout << "\n\n";

	Data d1{ {1, 2, 3}, {4, 5, 6}, "abc" };

	vector<Data> big_data;

	big_data.push_back(std::move(d1));

	REQUIRE(d1.row2.size() == 0);
}

template <typename T>
void deduce1(T arg)
{
	puts(__FUNCSIG__);
}

template <typename T>
void deduce2(T & arg)
{
	puts(__FUNCSIG__);
}

template <typename T>
void deduce3(T && arg)
{
	puts(__FUNCSIG__);
}

TEST_CASE("auto")
{
	int x = 10;
	const int cx = 20;
	int& rx = x;
	const int& crx = cx;
	int tab[] = { 1, 2, 3 };

	SECTION("case 1")
	{
		deduce1(x);
		auto ax1 = x; // int

		deduce1(cx);
		auto ax2 = cx; // int

		deduce1(rx);
		auto ax3 = rx; // int

		deduce1(crx);
		auto ax4 = crx; // int

		deduce1(tab);
		auto ax5 = tab;
	}

	SECTION("case 2")
	{
		deduce2(x);
		auto& ax1 = x; // int&

		deduce2(cx);
		auto& ax2 = cx; // const int&

		deduce2(rx);
		auto& ax3 = rx; // int&

		deduce2(crx);
		auto& ax4 = crx; // const int&

		deduce2(tab);
		auto& ax5 = tab; // int(&)[3]
	}

	SECTION("case 3")
	{
		deduce3(x);
		auto&& ax1 = x; // int&

		deduce3(cx);
		auto&& ax2 = cx; // const int&

		deduce3(rx);
		auto&& ax3 = rx; // int&

		deduce3(5);
		auto&& ax4 = 5; // int&&
	}
}

struct Gadget
{
	string name;
	int id;

	/*template <typename Arg>
	Gadget(Arg&& n, int id) : name{std::forward<Arg>(n)}, id{id}
	{}*/

	Gadget(std::string n, int id) : name{ std::move(n) }
	{
	}
};

template <typename T, typename Arg1, typename Arg2>
unique_ptr<T> my_make_unique(Arg1 && arg1, Arg2 && arg2)
{
	return std::unique_ptr<T>(new T{ std::forward<Arg1>(arg1), std::forward<Arg2>(arg2) });
}

TEST_CASE("my make unique")
{
	unique_ptr<Gadget> ptr_g = my_make_unique<Gadget>(string("ipad"), 1);

	string name = "mp3 player";

	Gadget g1{ "ipod", 3 };
	Gadget g2{ name, 4 };
}