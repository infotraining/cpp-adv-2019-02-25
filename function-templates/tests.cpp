#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <optional>

using namespace std;
using namespace Catch::Matchers;

template <typename T>
T my_max(T a, T b)
{
	cout << "Generic impl: ";
	puts(__FUNCSIG__);
	return a < b ? b : a;
}

template <typename T>
T* my_max(T* a, T* b)
{
	cout << "Pointer impl: ";
	puts(__FUNCSIG__);
	return *a < *b ? b : a;
}

const char* my_max(const char* a, const char* b)
{
	cout << "C-string impl: ";
	puts(__FUNCSIG__);
	return strcmp(a, b) < 0 ? b : a;
}

TEST_CASE("templates")
{    
	auto result = my_max(5, 10);

	REQUIRE(result == 10);

	auto x = 6;
	REQUIRE(my_max(4.5, static_cast<double>(x)) == Approx(6));
	REQUIRE(my_max<double>(4.5, x) == Approx(6));

	SECTION("pointers")
	{
		int x = 10;
		int y = 5;

		auto result = my_max(&x, &y);

		REQUIRE(*result == 10);
	}

	SECTION("cstring")
	{
		const char* txt1 = "Ala";
		const char* txt2 = "Ale";

		auto result = my_max(txt1, txt2);

		cout << result << endl;
	}
}


template <typename T1, typename T2>
struct Pair
{
	T1 first;
	T2 second;

	Pair(const T1& f, const T2& s) : first{f}, second{s}
	{}
};

template <typename T1, typename T2> Pair(T1, T2) -> Pair<T1, T2>;

TEST_CASE("Class Template Argument Deduction")
{
	Pair<int, double> p1{ 1, 3.14 };

	SECTION("Since C++17")
	{
		Pair p2{ 1, 3.14 };

		Pair p3{ 1, "one" };
	}

	SECTION("Special rule for deduction")
	{
		vector vec = { 1, 2, 3, 4 };

		vector vec2{vec};

		static_assert(is_same_v<decltype(vec2), vector<int>>);

		optional opt = 5;

		optional opt2 = opt;
	}
}

template <typename T>
struct Aggregate
{
	int x;
	T str;

	void print() const
	{
		cout << x << " " << str << endl;
	}
};

template <typename Ignore, typename T>
Aggregate(Ignore, T)->Aggregate<T>;

TEST_CASE("aggreagate {}")
{
	Aggregate agg{ 1, "text" };
}

struct WTF
{
	WTF() = delete;

	int x = 1;
};

TEST_CASE("WTF")
{
	WTF wtf{};

	REQUIRE(wtf.x == 1);
}