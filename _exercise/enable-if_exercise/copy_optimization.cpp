#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <iostream>
#include <list>
#include <string>
#include <vector>
#include <type_traits>

using namespace std;
using namespace Catch::Matchers;

enum class Implementation
{
    Generic,
    Optimized
};

template <typename InIter, typename OutIter>
Implementation mcopy(InIter start, InIter end, OutIter dest)
{
    for (auto it = start; it != end; ++it, ++dest)
    {
        *dest = *it;
    }

    return Implementation::Generic;
}

// TODO - optimized version with memcpy of mcopy
template <typename T>
constexpr bool IsMemCopyable_v = is_trivially_copyable<T>::value;

template <typename T1, typename T2>
constexpr bool AreTheSame_v = is_same<remove_cv_t<T1>, remove_cv_t<T2>>::value;

// optimized version
template <typename Src, typename Dest, typename = enable_if_t<IsMemCopyable_v<Src> && AreTheSame_v<Src, Dest>>>
Implementation mcopy(Src* start, Src* end, Dest* dest)
{
	auto length = end - start;

	memcpy(dest, start, length * sizeof(Src));

	return Implementation::Optimized;
}


TEST_CASE("mcopy")
{
    SECTION("generic version for STL containers")
    {
        vector<int> vec = {1, 2, 3, 4, 5};
        list<int> lst(5);

        REQUIRE(mcopy(vec.begin(), vec.end(), lst.begin()) == Implementation::Generic);
        REQUIRE(equal(vec.begin(), vec.end(), lst.begin(), lst.end()));
    }

    SECTION("generic for array of strings")
    {
        string words[] = {"1", "2", "3"};
        string dest[3];

        REQUIRE(mcopy(begin(words), end(words), begin(dest)) == Implementation::Generic);
        REQUIRE(equal(begin(words), end(words), begin(dest), end(dest)));
    }

    SECTION("optimized for arrays of POD types")
    {
        const int tab1[5] = {1, 2, 3, 4, 5};
        int tab2[5];

        REQUIRE(mcopy(begin(tab1), end(tab1), begin(tab2)) == Implementation::Optimized);
        REQUIRE(equal(begin(tab1), end(tab1), begin(tab2), end(tab2)));
    }
}