#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <cassert>
#include <iostream>
#include <type_traits>

using namespace std;

namespace vt
{
	struct Details
	{
		template <typename ResultT, typename T>
		constexpr static ResultT sum(T&& arg)
		{
			return arg;
		}

		template <typename ResultT, typename T, typename... Args>
		constexpr static ResultT sum(T&& arg, Args&& ... args)
		{
			return forward<T>(arg) + sum<ResultT>(forward<Args>(args)...);
		}
	};

	template <typename... Args>
	typename std::common_type<Args...>::type sum(Args&& ... args)
	{
		using ResultType = typename std::common_type<Args...>::type;

		return Details::sum<ResultType>(forward<Args>(args)...);
	}
}

namespace Cpp17
{
	template <typename... Args>
	typename std::common_type<Args...>::type sum(Args&& ... args)
	{
		return (... + std::forward<Args>(args));
	}
}

TEST_CASE("variadic sum")
{
    SECTION("for ints")
    {
        auto sum = Cpp17::sum(1, 3, 3);

        REQUIRE(sum == 7);
        static_assert(is_same<int, decltype(sum)>::value, "Error");
    }

    SECTION("for floating points")
    {
        auto dbl_sum = Cpp17::sum(1.1, 3.0f, 3);

        REQUIRE(dbl_sum == Approx(7.1));
        static_assert(is_same<double, decltype(dbl_sum)>::value, "Error");
    }

    SECTION("for strings")
    {
        auto text = Cpp17::sum("Hello", string("world"), "!");

        REQUIRE(text == "Helloworld!");
        static_assert(is_same<string, decltype(text)>::value, "Error");
    }
}