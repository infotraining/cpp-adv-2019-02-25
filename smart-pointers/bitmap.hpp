#pragma once
#include <memory>

class Bitmap
{
	struct Impl;

	std::unique_ptr<Impl> impl_;
public:
	Bitmap();
	~Bitmap();
	void draw() const;	

};
