#include "catch.hpp"
#include <iostream>
#include <string>
#include <vector>
#include "bitmap.hpp"

using namespace std;
using namespace Catch::Matchers;

void may_throw()
{
	throw std::runtime_error("ERROR#13");
}

TEST_CASE("unique_ptr custom dealloc")
{
	try
	{
		//unique_ptr<FILE, int(*)(FILE*)> f{ fopen("text.abc", "w"), &fclose };

		auto file_closer = [](FILE * f) { fclose(f); };
		unique_ptr<FILE, decltype(file_closer)> f{ fopen("text.abc", "w"), file_closer };

		fprintf(f.get(), "text");

		may_throw();
	}
	catch (const std::runtime_error & e)
	{
		cout << "Caught an error: " << e.what() << endl;
	}
}

TEST_CASE("shared_ptr custom dealloc")
{
	shared_ptr<FILE> shared_file{ fopen("text.abc", "w") , [](FILE * f) { if (f) fclose(f); } };

	fprintf(shared_file.get(), "text");
}

TEST_CASE("CTAD")
{
	vector vec = { 1, 2, 3, 4 };

	//unique_ptr up{ new int[13] }; // ill formed

	Bitmap bmp;
	bmp.draw();
}