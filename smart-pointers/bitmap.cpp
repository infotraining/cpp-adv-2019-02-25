#include "bitmap.hpp"
#include <iostream>

using namespace std;

struct Bitmap::Impl
{
	char* pixels_;
	size_t size_;
};

Bitmap::Bitmap() 
	: impl_{make_unique<Bitmap::Impl>()}
{
	impl_->pixels_ = new char[10];
	impl_->size_ = 10;

	std::fill_n(impl_->pixels_, impl_->size_, '*');
}

Bitmap::~Bitmap() {
	delete[] impl_->pixels_;
}

void Bitmap::draw() const
{
	for (size_t i = 0; i < impl_->size_; ++i)
		cout << impl_->pixels_[i];
	cout << endl;
}